#include "Queue.h"
#include <vector>
#include <iostream>
#include <queue>

void CreateQueue::push(int x)
{
	myMutex.lock();
	myQueue.push(x);
	myMutex.unlock();
}
void CreateQueue::pop()
{
	myMutex.lock();
	myQueue.pop();
	myMutex.unlock();
}
int CreateQueue::front()
{
	myMutex.lock();
	int x = myQueue.front();
	myMutex.unlock();
	return x;
}
