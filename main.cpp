#include <iostream>
#include <thread>
#include <vector>
#include <memory>
#include "Queue.h"
#include "computePi.hpp"
#include "Map.h"
#include <queue>



void threadWorker(CreateQueue &myQueue, Map &myMap ) {


   // Take First value off queue
    while(!myQueue.myQueue.empty())
    {
        int key = myQueue.myQueue.front();
        myQueue.myQueue.pop();
        int value = computePiDigit(key);
        myMap.insert(key,value);
        std::cout << ".";
        std::cout.flush();

    }
    //Call Compute Pi
    //Insert into undordered map.
}



int main() {


    CreateQueue queueOfIntegers;
    Map mapOfValues;
    for(int i = 1; i <= 1000; i++)
    {
        queueOfIntegers.myQueue.push(i);
    }

	//
	// Make as many threads as there are CPU cores
    // Assign them to run our threadWorker function, and supply arguments as necessary for that function
	std::vector<std::shared_ptr<std::thread> > threads;
	for (std::uint16_t core = 0; core < std::thread::hardware_concurrency(); core++)
        // The arguments you wish to pass to threadWorker are passed as
        // arguments to the constructor of std::thread
		threads.push_back(std::make_shared<std::thread>(threadWorker, std::ref(queueOfIntegers), std::ref(mapOfValues)));

	//
	// Wait for all of these threads to complete
	for (auto&& thread : threads)
		thread->join();

	std::cout << std::endl << std::endl;

    std::cout << "3.";
    for (int j = 1; j <= 1000; ++j)
    {
        std::cout << mapOfValues.find(j);
    }
    std::cout << std::endl;


	return 0;
}
