#include <iostream>
#include <thread>
#include <memory>
#include <queue>
#include <deque>
#include <mutex>

#ifndef QUEUE_H
#define QUEUE_H

class CreateQueue{
	
public:
	std::queue<int> myQueue;
	std::mutex myMutex;
	void push(int);
	void pop();
	int front();
	bool isEmpty();
};


#endif
