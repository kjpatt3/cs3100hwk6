#include <iostream>
#include <thread>
#include <memory>
#include <queue>
#include <deque>
#include <mutex>
#include <unordered_map>

#ifndef MAP_H
#define MAP_H

class Map{
	
public:
    std::unordered_map<int, int> myMap;
	std::mutex myMutex;
	void insert(int x, int y);
    int find(int);
    int key;
    int value;
		
};

void Map::insert(int x, int y)
{
    myMutex.lock();
    key = x;
    value = y;
    myMap.insert({key,value});
    myMutex.unlock();
}
int Map::find(int key)
{
    myMutex.lock();
    auto value = myMap[key];
    myMutex.unlock();
    return value;

}




#endif

